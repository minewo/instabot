#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time

from src import InstaBot
from src.check_status import check_status
from src.feed_scanner import feed_scanner
from src.follow_protocol import follow_protocol
from src.unfollow_protocol import unfollow_protocol

#主な設定箇所

#login:user:自分のアカウント名
#passward:自分のパスワード
#like_per_day:1日にいいねする数
#comments_per_day:1日にコメントする数
#tag_list:いいねするハッシュタグリスト
#tag_blacklist:入っていたら、いいねしないハッシュタグリスト
#max_like_for_one_tag:１つのタグに対して最大どのくらいいいねするか
#follow_per_day:1日にフォローする数
#unfollow_per_day:1日にアンフォローする数
#unfollow_break_min:最小でどのくらいフォロワーを減らすか
#unfollow_break_max:最大でどのくらいフォロワーを減らすか
#log_mod:0=コンソールにログを表示　1=ファイルにログを残す　2=ログ残さない

bot = InstaBot(
    login="cat_tubuyaki",
    password="s977043",
    database_name="cat_db.db",
    like_per_day=1000,
    comments_per_day=0,
#    tag_list=['follow4follow', 'f4f', 'cute', 'l:212999109'],
    tag_list=['fashion','fashionblogger','model','pasha_magazine ','portrait','SORA','withgirls_jp','おしゃれな人と繋がりたい','お洒落さんと繋がりたい','カメラマンさんと繋がりたい','カメラマンと繋がりたい','カメラマン依頼','カメラマン募集','カメラ好きな人と繋がりたい','サロモ','パーカー','パーカー好き','パーカー女子','ファインダー越しの私の世界','ファション','フォトグラファー','ヘッドフォン女子','ヘッドホン女子','ポートレイト','ポートレート','モデル','モデル撮影','モデル募集','作品撮り','作品撮影','撮影依頼','写真を撮っている人と繋がりたい','写真好きと繋がりたい','写真撮ってる人と繋がりたい','東京カメラ部 ','東京カメラ部Instagram','被写体','被写体募集','モデル','作品撮り','ポートレート','東京カメラ部','ポートレート部','フォトジェニック','インスタばえ','インスタジェニック','ハーレークイン','ハーレイクイン','ハロウィン','ハロウィンコスプレ','コスプレ','instajapan','SuicideSquad','harleyquinncostume','harleyquinncosplay','harleyquinn','Halloweencostume','Halloweencosplay','cosplayportrait','portraitphotography','good_portraits_world','instagramjapan','ig_japan','IGersJP','pasha_magazine'],
    tag_blacklist=['rain', 'thunderstorm'],
    user_blacklist={},
    media_max_like=800,
    media_min_like=120,
    max_like_for_one_tag=50,
    follow_per_day=0,
    follow_time=1 * 60,
    unfollow_per_day=0,
    unfollow_break_min=5,
    unfollow_break_max=15,
    log_mod=1,
    proxy='',
    # List of list of words, each of which will be used to generate comment
    # For example: "This shot feels wow!"
#    comment_list=[["this", "the", "your"],
#                  ["photo", "picture", "pic", "shot", "snapshot"],
#                  ["is", "looks", "feels", "is really"],
#                  ["great", "super", "good", "very good", "good", "wow",
#                   "WOW", "cool", "GREAT","magnificent", "magical",
#                   "very cool", "stylish", "beautiful", "so beautiful",
#                   "so stylish", "so professional", "lovely",
#                   "so lovely", "very lovely", "glorious","so glorious",
#                   "very glorious", "adorable", "excellent", "amazing"],
#                  [".", "..", "...", "!", "!!", "!!!"]],
    comment_list=[],
    # Use unwanted_username_list to block usernames containing a string
    ## Will do partial matches; i.e. 'mozart' will block 'legend_mozart'
    ### 'free_followers' will be blocked because it contains 'free'
#    unwanted_username_list=[
#        'second', 'stuff', 'art', 'project', 'love', 'life', 'food', 'blog',
#        'free', 'keren', 'photo', 'graphy', 'indo', 'travel', 'art', 'shop',
#        'store', 'sex', 'toko', 'jual', 'online', 'murah', 'jam', 'kaos',
#        'case', 'baju', 'fashion', 'corp', 'tas', 'butik', 'grosir', 'karpet',
#        'sosis', 'salon', 'skin', 'care', 'cloth', 'tech', 'rental', 'kamera',
#        'beauty', 'express', 'kredit', 'collection', 'impor', 'preloved',
#        'follow', 'follower', 'gain', '.id', '_id', 'bags'
    unwanted_username_list=[
    ],
#    unfollow_whitelist=['example_user_1', 'example_user_2'])
    unfollow_whitelist=[])

while True:

    #print("# MODE 0 = ORIGINAL MODE BY LEVPASHA")
    #print("## MODE 1 = MODIFIED MODE BY KEMONG")
    #print("### MODE 2 = ORIGINAL MODE + UNFOLLOW WHO DON'T FOLLOW BACK")
    #print("#### MODE 3 = MODIFIED MODE : UNFOLLOW USERS WHO DON'T FOLLOW YOU BASED ON RECENT FEED")
    #print("##### MODE 4 = MODIFIED MODE : FOLLOW USERS BASED ON RECENT FEED ONLY")
    #print("###### MODE 5 = MODIFIED MODE : JUST UNFOLLOW EVERYBODY, EITHER YOUR FOLLOWER OR NOT")

    ################################
    ##  WARNING   ###
    ################################

    # DON'T USE MODE 5 FOR A LONG PERIOD. YOU RISK YOUR ACCOUNT FROM GETTING BANNED
    ## USE MODE 5 IN BURST MODE, USE IT TO UNFOLLOW PEOPLE AS MANY AS YOU WANT IN SHORT TIME PERIOD

    mode = 0

    print("You choose mode : %i" %(mode))
    print("CTRL + C to cancel this operation or wait 10 seconds to start")
    time.sleep(10)

    if mode == 0:
        bot.new_auto_mod()

    elif mode == 1:
        check_status(bot)
        while bot.self_following - bot.self_follower > 200:
            unfollow_protocol(bot)
            time.sleep(10 * 60)
            check_status(bot)
        while bot.self_following - bot.self_follower < 400:
            while len(bot.user_info_list) < 50:
                feed_scanner(bot)
                time.sleep(5 * 60)
                follow_protocol(bot)
                time.sleep(10 * 60)
                check_status(bot)

    elif mode == 2:
        bot.bot_mode = 1
        bot.new_auto_mod()

    elif mode == 3:
        unfollow_protocol(bot)
        time.sleep(10 * 60)

    elif mode == 4:
        feed_scanner(bot)
        time.sleep(60)
        follow_protocol(bot)
        time.sleep(10 * 60)

    elif mode == 5:
        bot.bot_mode = 2
        unfollow_protocol(bot)

    else:
        print("Wrong mode!")
